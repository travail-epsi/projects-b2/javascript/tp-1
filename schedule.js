lessons()

function displaySkeletton(weeks) {
    const separator = "|----------|-------|-------|----------|-------|----------|";

    displayHeader(separator);
    displayLines(separator, weeks)
}


function displayHeader(separator) {
    const days = ["         ", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi"];
    let first = "|"

    for (let column = 0; column < days.length; column++) {
        first += `${days[column]} | `
    }
    console.log(separator)
    console.log(first)
    console.log(separator)
}

function displayLines(separator, weeks) {
    const hours = ["8h-10h ", "10h-12h", "14h-16h", "16h-18h"];
    let line = ""

    for (let column = 0; column < hours.length; column++) {
        //line -> cours pour un créneau horaires chaque jours
        line += `| ${hours[column]}  |${weeks[column][0]} |${weeks[column][1]} |  ${weeks[column][2]}  |${weeks[column][3]} |  ${weeks[column][4]}  |\n${separator}\n`
    }
    console.log(line)
}


function create() {
    let days = [5]; // Taille 5 -> jours de la semaine
    let weeks = [4]; // Taille 4 -> créneaux horaires
    for (let i = 0; i < weeks.length; i++) {
        for (let j = 0; j < days.length; j++) {
            //Remplissage de la ligne => chercher le nb d'heures dans teachers => placer dans le tableau + soustraire dans le json
            days[j] = lessons();
        }
        weeks.push(days)
        days = [5];
    }


    displaySkeletton(weeks)
}

function lessons(className) {
    //Objet permettant de faire le lien entre les profs, les cours et le nombre d'heures par cours
    let teachers = {
        P1: {
            0: {
                RF1: 20
            },
            1: {
                BO8: 8
            },
            2: {
                AF4: 50
            },
        },
        P2: {
            0: {
                SE1: 80
            },
            1: {
                BU1: 14
            },
        },
        P3: {
            0: {
                MO1: 30
            },
        },
        P4: {
            0: {
                PK5: 60
            },
        },
        P5: {
            0: {
                NI8: 10
            },
            1: {
                AD6: 48
            },
            2: {
                BG5: 18
            }
        },
        P6: {
            0: {
                FG8: 26
            }
        },
        P7: {
            0: {
                GR5: 36
            },
        },
        P8: {
            0: {
                TR7: 30
            }
        },
    }
    /**
     * Ces constantes sont pour aider à respecter les règles de gestions
     */
    const lessonHoursNBPerDays = 2;
    const maxHoursPerClass = 8;
    // Le 9 est en dur car on sait que dans ce cas là, on a pas plus de 8 profs
    for (let teacherNumber = 1; teacherNumber < 9; teacherNumber++) {
        //On créer nous même la variable du prof
        let teacher = `P${teacherNumber}`;
        // Le 3 est en dur car on sait que dans ce cas là, on a pas plus de 3 cours par prof
        for (let lessonNumber = 0; lessonNumber < 3; lessonNumber++) {
            // Vu que les profs ont pas le même nombre de cours, on vérifie si le prof a assez de cours. Sinon on passe au prof suivant
            if (!teachers[teacher][lessonNumber]) break;
            /**
             * Traitement pour savoir quel cours on met quand en respectant les règles de gestions
             */
        }
    }


}